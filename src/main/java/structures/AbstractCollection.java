package structures;

public abstract class AbstractCollection <T> {
    public abstract int size();
}
